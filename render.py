#!/bin/env python

import os
from pathlib import Path
import urllib.request

import django
from django.template.loader import render_to_string
from django.conf import settings
from tqdm import tqdm

from data import units_db


settings.configure(
    TEMPLATES=[
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [os.path.dirname(os.path.realpath(__file__))],
            'APP_DIRS': True,
        },
    ]
)
django.setup()

public_dir = Path('public')
imgs_folder = public_dir / 'imgs'
units = units_db()
for unit in tqdm(list(units.values())):
    # Remove units without counters.
    if not unit.get('strong') and not unit.get('weak'):
        units.pop(unit['id'])
        continue

    # Replace strings with full unit data.
    for kind in ['strong', 'weak']:
        unit[kind] = [units[id_] for id_ in unit.get(kind, []) if id_ in units]

    # Download images (Liquidpedia doesn't allow direct link :_-( ).
    img_path = (imgs_folder / unit['name']).with_suffix(Path(unit['image']).suffix)
    if not img_path.exists():
        urllib.request.urlretrieve(unit['image'], img_path)
    unit['image'] = img_path.relative_to(public_dir)

text = render_to_string('tpl.html', {'units': units.values()})
(public_dir / 'index.html').write_text(text, encoding='utf8')
