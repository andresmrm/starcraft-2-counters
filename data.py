import re
import time
import shelve

import mwclient
import mwparserfromhell as parser
from tqdm import tqdm


def get_page(link: str, img=False):
    '''Cached page get and parse.'''
    text = None
    with shelve.open('cache.shelve') as db:
        if link in db:
            text = db[link]
        else:
            site = mwclient.Site("liquipedia.net/starcraft2", path="/")
            if img:
                text = site.images[link].imageinfo['url']
            else:
                page = site.pages[link]
                text = page.text()
            db[link] = text
            time.sleep(2)

    if not img:
        return parser.parse(text)
    return text


def norm_id(s):
    '''Normalize id string.'''
    if not isinstance(s, str):
        s = s.strip_code()
    return re.sub(r"\s+\(.+\)\s*", '', s.lower())


def get_units_links():
    '''Units page links for each race.'''
    parsed = get_page("Template:Units_and_Buildings_in_Legacy_of_the_Void_navbox")
    navbox = parsed.filter_templates()[0]

    def extract(i):
        return [
            l.title.strip_code()
            for l in navbox.params[-i]
            .value.filter_templates()[0]
            .params[-3]
            .value.filter_wikilinks()
        ]

    return {
        'protoss': extract(3),
        'humans': extract(2),
        'zergs': extract(1),
    }


def parse_counters(data, kind):
    '''Normalize data about strong and weak.'''
    if kind in data:
        data[kind] = set(norm_id(l.title) for l in data[kind].filter_wikilinks())


def get_unit_data(link):
    '''Get unit's data from page.'''
    parsed = get_page(link)
    for tpl in parsed.filter_templates():
        if tpl.name.contains('Infobox'):
            data = dict((str(p.name), p.value) for p in tpl.params)
            data['name'] = data['name'].strip_code()
            parse_counters(data, 'strong')
            parse_counters(data, 'weak')
            data['image'] = get_page(data['image'].strip_code(), img=True)
            return data


def expand_counters(units, unit, kind):
    if kind in unit:
        for link in unit[kind]:
            try:
                other = units[link]
            except KeyError:
                print(f'Not found: {link} for {unit["name"]}.')
                continue

            cc = other.get('strong' if kind == 'weak' else 'weak', set())
            cc.add(unit['id'])


def units_db() -> dict:
    '''Returns a dict with data about all units.'''
    units = {}
    units_links = get_units_links()

    for race, links in tqdm(units_links.items()):
        for link in tqdm(links, leave=False):
            data = get_unit_data(link)
            data['race'] = race
            id_ = norm_id(link)
            data['id'] = id_
            units[id_] = data

    for unit in units.values():
        expand_counters(units, unit, 'strong')
        expand_counters(units, unit, 'weak')

    return units
